<?php

class Personnage {
    //private $nom = "manelle";
    protected $nom;
    public $genre;
    public $taille;
    public $intel;
    public $force;
    public $dexterite;
    public $apparence;
    public $vitesse;
    public $attaque;
    public $defense;

    protected const AVATAR = "images/common/avatar.jpg"; 

    public function __construct($n, $g){
        $this->nom = $n ;
        $this->genre = $g ; 
    }

    public function __destruct(){
        //ici on fait des trucs avant que l'objet soit détruit
    }


    //getter
    public function get_nom() {
        return $this->nom ;
    }
    //setter
    public function set_nom($n){
        $this->nom = $n;
    }
}

class Magicien extends Personnage {
    protected const AVATAR = "images/magicien/avatar.jpg"; 
    //const = constante

    public function get_nom(){
        return "*** ".$this->nom." ***";
    }

    public static function get_avatar(){ //si c'est privé
        return self::AVATAR;
    }

    // public static function get_avatar(){
    //     return parent::AVATAR ;
    // }

}


//echo Personnage::AVATAR; //si c'est public

//fontion anonyme :
// 1/auto invocation
(function (){
    echo "execution";
})();


/////////////////////////////////////////////////////
// 2/passage en parametre
$squ = function(float $x){
    return $x**2;
};

//Définition d'un tableau
$tb = [1, 2, 3, 4, 5];

//array_map() exécute notre closure sur chaque élément du tableau
$tb_squ = array_map($squ, $tb);
echo '<pre>';
print_r($tb_squ);
echo '</pre>';




///////////////////////////////////////////////////
$txt = function(){
    echo 'Fonction anonyme bien exécutée';
};

$squ = function(float $x){
    return 'Le carré de ' .$x. ' est ' .$x**2;
};

$txt(); //methode magique invoke
echo '<br>';
echo $squ(3);

//////////////////////////////////////


$anonyme = new class{
    public $user_name;
    public const BONJOUR = 'Bonjour ';
    
    public function setNom($n){
        $this->user_name = $n;
    }
    public function getNom(){
        return $this->user_name;
    }
};

$anonyme->setNom('Pierre');
echo $anonyme::BONJOUR;
echo $anonyme->getNom();
echo '<br><br>';

//Affiche les infos de $anonyme
var_dump($anonyme);



///////////////////////////////////////////



function anonyme(){
    return new class{
        public $user_name;
        public const BONJOUR = 'Bonjour ';
    
        public function setNom($n){
            $this->user_name = $n;
        }
        public function getNom(){
            return $this->user_name;
        }
    };
}

$anonyme = anonyme();
$anonyme->setNom('Pierre');
echo $anonyme::BONJOUR;
echo $anonyme->getNom();
echo '<br><br>';