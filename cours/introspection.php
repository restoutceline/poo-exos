<?php
/* introspection des objets 
class_exists() – checks whether a class has been defined
get_class() – returns the class name of an object
get_parent_class() – returns the class name of an object’s parent class
is_subclass_of() – checks whether an object has a given parent class
*/
class Introspection
{
    public function description() {
        echo "I am a super class for the Child class.n";
    }
}
class Child extends Introspection
{
    public function description() {
        echo "I'm " . get_class($this) , " class.n";
        echo "I'm " . get_parent_class($this) , "'s child.n";
    }
}
if (class_exists("Introspection")) {
    $introspection = new Introspection();
    echo "The class name is: " . get_class($introspection) . "n"; 
    $introspection->description();
}
if (class_exists("Child")) {
    $child = new Child();
    $child->description();
    if (is_subclass_of($child, "Introspection")) {
        echo "Yes, " . get_class($child) . " is a subclass of Introspection.n";
    }
    else {
        echo "No, " . get_class($child) . " is not a subclass of Introspection.n";
    }
}
/* display 
The class name is: Introspection
I am a super class for the Child class.
I'm Child class.
I'm Introspection's child.
Yes, Child is a subclass of Introspection.
*/

/*
get_declared_classes() – returns a list of all declared classes
get_class_methods() – returns the names of the class’ methods
get_class_vars() – returns the default properties of a class
interface_exists() – checks whether the interface is defined
method_exists() – checks whether an object defines a method
*/

interface ICurrencyConverter
{
    public function convert($currency, $amount);
}
class GBPCurrencyConverter implements ICurrencyConverter
{
    public $name = "GBPCurrencyConverter";
    public $rates = array("USD" => 0.622846,
                          "AUD" => 0.643478);
    protected $var1;
    private $var2;
    function __construct() {}
    function convert($currency, $amount) {
        return $rates[$currency] * $amount;
    }
}
if (interface_exists("ICurrencyConverter")) {
    echo "ICurrencyConverter interface exists.n";
}
$classes = get_declared_classes();
echo "The following classes are available:n";
print_r($classes);
if (in_array("GBPCurrencyConverter", $classes)) {
    print "GBPCurrencyConverter is declared.n";
 
    $gbpConverter = new GBPCurrencyConverter();
    $methods = get_class_methods($gbpConverter);
    echo "The following methods are available:n";
    print_r($methods);
    $vars = get_class_vars("GBPCurrencyConverter");
    echo "The following properties are available:n";
    print_r($vars);
    echo "The method convert() exists for GBPCurrencyConverter: ";
    var_dump(method_exists($gbpConverter, "convert"));
}
/* display 
ICurrencyConverter interface exists.
The following classes are available:
Array
(
    [0] => stdClass
    [1] => Exception
    [2] => ErrorException
    [3] => Closure
    [4] => DateTime
    [5] => DateTimeZone
    [6] => DateInterval
    [7] => DatePeriod
    ...
    [154] => GBPCurrencyConverter
)

GBPCurrencyConverter is declared.
The following methods are available:
Array
(
    [0] => __construct
    [1] => convert
)
The following properties are available:
Array
(
    [name] => GBPCurrencyConverter
    [rates] => Array
        (
            [USD] => 0.622846
            [AUD] => 0.643478
        )
)
The method convert() exists for GBPCurrencyConverter: bool(true)
*/
$child = new ReflectionClass("Child");
$parent = $child->getParentClass();
echo $child->getName() . " is a subclass of " . $parent->getName() . ".n";
$reflection = new ReflectionClass("GBPCurrencyConverter");
$interfaceNames = $reflection->getInterfaceNames();
if (in_array("ICurrencyConverter", $interfaceNames)) {
    echo "GBPCurrencyConverter implements ICurrencyConverter.n";
}
$methods = $reflection->getMethods();
echo "The following methods are available:n";
print_r($methods);
if ($reflection->hasMethod("convert")) {
    echo "The method convert() exists for GBPCurrencyConverter.n";
}