<?php
/*
comparaison d'objets :
if obj == ou === obj2
mêmes attributs / mêmes valeurs
même instances de classe
utiliser instanceof
*/
            class Utilisateur{
                protected $nom;
                public function __construct($n){
                    $this->nom = $n;
                }
                public function __clone(){
                    $this->nom = $this->nom. ' (clone)';
                }
                public function getNom(){
                    echo $this->nom;
                }
                public function setNom($new_name){
                    $this->nom = $new_name ;
                }
            }
            $pierre = new Utilisateur('Pierre');
            $pierre2 = new Utilisateur('Pierre');
            $jean = $pierre; 
            $victor = clone $pierre;

            $pierre->setNom("Jack");
            echo"<pre>";
            var_dump($pierre);
            var_dump($jean);
            var_dump($victor);
            
           
           /*
            echo 'Egalité simple entre $pierre et $pierre2 ? ';
            var_dump($pierre == $pierre2);
            echo '<br>Identité entre $pierre et $pierre2 ? ';
            var_dump($pierre === $pierre2);
            echo '<br><br>Egalité simple entre $pierre et $jean ? ';
            var_dump($pierre == $jean);
            echo '<br>Identité entre $pierre et $jean ? ';
            var_dump($pierre === $jean);
            echo '<br><br>Egalité simple entre $pierre et $victor ? ';
            var_dump($pierre == $victor);
            echo '<br>Identité entre $pierre et $victor ? ';
            var_dump($pierre === $victor);
            */