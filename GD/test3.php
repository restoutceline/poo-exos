<?php


header ("Content-type: image/png");

$x = 300;
$y = 300;

$image = imagecreatetruecolor($x,$y);

//je récupère une chaîne
// $color = "FF0000";
$color = $_GET["color"];

//
$rouge = hexdec(substr($color,0,2));
$vert = hexdec(substr($color,2,4));
$bleu = hexdec(substr($color,4,6));

$couleur = imagecolorallocate($image, $rouge, $vert, $bleu);

imagefill($image,0,0,$couleur);

imagepng($image);

imagedestroy($image);
