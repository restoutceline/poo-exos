<?php

header("Content-type: image/png"); //on envoie les infos au navigateur
$source = imagecreatefrompng("imagecopy.png"); //on ouvre l'image source
$destination = imagecreatetruecolor(300,300); //on crée une image truecolor de 300x300 pixels
imagecopy($destination,$source, 0, 0, 125,132, 137, 136); //on copie un morceau de l'image
imagecopy($destination,$source, 150, 150, 40, 80, 137, 136); //et un autre
imagepng($destination);
imagedestroy($destination, $source);