<?php

abstract class Histo {
    // un tableau qui sert a stocker les données
    protected $data= [];
    // une fct qui sert a ajouter des valeurs 
    public function addValue($val) {
        array_push($this->data, $val);
    }

    public function addValues($arr){
        $this->data = $arr ;
    }
    // une fct qui sert a dessiner
    public function draw($w,$h) {
        // on calcule le nb de colonnes
       $col = count($this->data);
       // on deduit la largeur moyenne de chaque colonne 
       $col_width = $w / $col ; 
       // on enleve la distance entre entre 2 colonnes 
       $pad = $col_width/4 ;
       // on rajoute un offset pour les legendes 
       $bottom =20 ;
       // on crée l'image 
       $im = imagecreate($w,$h);
       // on crée la palette  
       $blue = imagecolorallocate($im,0,102,255);
       $gray = imagecolorallocate($im,50,50,50);
       $white = imagecolorallocate($im,255,255,255);
        // on crée le fond de l'image 
       imagefilledrectangle($im,0,0,$w,$h,$white);
        // on calcule la valeur max des données 
      $maxv = 0 ;
      for($i=0;$i<$col;$i++){
        $maxv = max($this->data[$i],$maxv);
      }
      // on construit les barres 
      for($i=0;$i<$col;$i++){
        $col_height = ( $h  ) * ( $this->data[$i] / $maxv );
        $x1 = ($i*$col_width)+($pad/2);
        $y1 = $h-$col_height-$bottom ;
        $x2 = (($i+1)*$col_width)-$pad+$pad/2 ;
        $y2 = $h - $bottom ;
        imagefilledrectangle($im,$x1,$y1,$x2,$y2,$blue);
        imagestring($im,2,$x1+$pad,$h-14,$this->data[$i],$gray);
      }
     
      imageline($im,0,$h-$bottom,$w,$h-$bottom,$gray);
      $texte = 'mon histogramme' ;
      
       header("Content-type: image/png");
       imagepng($im);
    }
}

class Histograph extends Histo {

}

$mydata = [231,34,56,156,100,300,12,125] ;
$myhisto = new Histograph ;
$myhisto->addValues($mydata);
$myhisto->draw(600,400);
