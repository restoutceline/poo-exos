<?php

header('Content-type: image/png');
$ratio = .5;
// Calcul des nouvelles dimensions
list($largeur, $hauteur) = getimagesize("imagecopy.png"); //list est un moyen plus pratique pour ne récupérer que ce qu'on veut
$n_largeur = $largeur * $ratio;
$n_hauteur = $hauteur * $ratio;
//création de la destination
$destination = imagecreatetruecolor($n_largeur, $n_hauteur);
//on ouvre la source
$source = imagecreatefrompng("imagecopy.png");
// Redimensionnement
imagecopyresampled($destination, $source, 0, 0, 0, 0, $n_largeur, $n_hauteur, $largeur, $hauteur);
imagepng($destination);
imagedestroy($destination);
imagedestroy($source);