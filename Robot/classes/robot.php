<?php

class Robot {

    private $name;
    private $x;
    private $y;
    private $orientation;
    private $size;
    private $color;
    private $action;
    private $autonomie;
    private $max_speed;
    private $arena;

    public function __construct ($n, $x, $y, $o, $s, $c, $a, $au, $m, $ar){
        $this->name = $n;
        $this->x = $x;
        $this->y = $y;
        $this->orientation = $o;
        $this->size = $s;
        $this->color = $c;
        $this->action = $a;
        $this->autonomie = $au;
        $this->max_speed = $m;
        $this->arena = $ar;
        array_push($this->arena->bots, $this); //bots à array
    }

    public function get_position(){
        return [$this->x, $this->y, $this->orientation] ;
    }

    public function move($longueurx, $longueury){
        $this->x += $longueurx;
        $this->y += $longueury;
        $this->arena->get_bot_pos($bot1) ;
    }

    public function turn($orientation){
        $this->orientation = $orientation ;
    }

    public function exec($action){

    }
    
    public function speak($action){

    }
}

class arena {
    private $name ;
    private $sizex; // en pixel ?
    private $sizey;
    private $bots = [];

    public function __construct($n, $sx, $sy){
        $this->name = $n;
        $this->sizex = $sx;
        $this->sizey = $sy;
    }

    public function get_bot_pos ($bot){
        $this->$bot = $bot ;
       
    }
}


$bot1 = new Robot("Philippe", 0, 0, 45, "small", "blue", "collect", 2, 2, $arena1);
//echo $bot1->get_position(); //control d'affichage
//echo "<pre>";
//var_dump($bot1->get_position());
//$bot1->turn(90);
//$bot1->move(50,50);
//var_dump($bot1->get_position());

$arena1 = new Arena ("KEKW Arena", 50, 50); //supposé etre une div

$bot2 = new Robot("Michel", 50, 50, 225, "small", "red", "run", 2, 100, $arena1);
$bot1->move(50,50);

var_dump($arena1);