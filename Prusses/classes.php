<?php

/**
 * On veut modéliser le fonctionnement d’un ensemble de poupées russes. Une poupée russe est une sorte de boîte.
 * Il en existe de différentes tailles et on peut placer une poupée dans une autre plus grande
 * dès lors que la plus petite est fermée et la plus grande ouverte.
 * 
 * 1. Quels sont les caractéristiques qui définissent une poupée russe ou son état ?
 * 2. Ecrire la classe PoupeeRusse contenant les méthodes suivantes
 * a. Constructeur
 * b.  ouvrir() : ouvre la poupée si elle n’est pas déjà ouverte et si elle ne se
 * trouve pas à l’intérieur d’une autre poupée
 * c.  fermer() : ferme la poupée si elle n’est pas déjà fermée et si elle ne se
 * trouve pas à l’intérieur d’une autre poupée
 * d.  placerDans(PoupeeRusse p) : place la poupée courante dans la poupée p
 * si elle n’est pas déjà dans une autre et si la poupée p ne contient aucune autre
 * poupée et si la poupée courante est fermée et la poupée p est ouverte et plus grande
 * e.  sortirDe(PoupeeRusse p) : sort la poupée courante de la poupée p si elle
 * est dans p et si p est ouverte
 * Vous devez ajouter les attributs dont vous avez besoin.
 */

 class PoupeeRusse {

    function __constructor(){

    }

    function ouvrir(){

    }

    function fermer(){

    }

    function placerDans(){

    }

    function sortirDe(){

    }

 }